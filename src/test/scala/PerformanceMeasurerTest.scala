import com.perceptron.neural_network.constants.ActivationFunctionEnum
import com.perceptron.neural_network.{NeuralNetwork, NeuralNetworkInterface, PerformanceMeasurer}
import com.perceptron.neural_network.constants.VerbosityEnum.Verbosity
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.PrivateMethodTester

class NnMock(returnValue: Vector[Double]) extends NeuralNetworkInterface {
  override def fit(X: Vector[Vector[Double]], y: Vector[Vector[Double]],
                   epochs: Int, batchSize: Int, verbosity: Verbosity,
                   verboseRate: Int): NeuralNetwork = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)

  override def predict(X: Vector[Double]): Vector[Double] = returnValue
}


class PerformanceMeasurerTest extends AnyFlatSpec with PrivateMethodTester {
  val predictedValues: Vector[Double] = Vector(1, 2, 3, 4, 5, 6, 7, 8, 9.0)
  implicit val nn: NnMock with PerformanceMeasurer = new NnMock(predictedValues) with PerformanceMeasurer
  val XData: Vector[Vector[Double]] = Vector(Vector(1.0), Vector(2.0))
  val yData: Vector[Vector[Double]] = Vector(Vector(1, 1, 1, 1, 1, 1, 1, 1, 1.0), Vector(1, 1, 1, 1, 1, 1, 1, 1, 1.0))


  "PerformanceMeasurer" should "calculate calculateLoss correctly" in {
    val res = nn.calculateLoss(Vector(1), Vector(1, 1, 1, 1, 1, 1, 1, 1, 1.0))
    assert(res == 204.0)
  }

  "PerformanceMeasurer" should "calculate loss correctly" in {
    trait PerformanceMeasurerCalculateLossMock extends PerformanceMeasurer {
      override def calculateLoss(X: Vector[Double], y: Vector[Double])(implicit nn: NeuralNetworkInterface): Double = {
        (X, y) match {
          case (Vector(1.0), Vector(1, 1, 1, 1, 1, 1, 1, 1, 1.0)) => 0.5
          case (Vector(2.0), Vector(1, 1, 1, 1, 1, 1, 1, 1, 1.0)) => 1
        }
      }
    }
    val nn = new NnMock(predictedValues) with PerformanceMeasurerCalculateLossMock
    val res = nn.loss(XData, yData)(nn)
    assert(res == 0.75)
  }

  "PerformanceMeasurer" should "calculate accuracy correctly" in {
    val predictedValues = Vector(1, 2, 3, 4, 5, 6, 7, 8, 9.0)
    implicit val nn: NnMock with PerformanceMeasurer = new NnMock(predictedValues) with PerformanceMeasurer
    val res = nn.accuracy(XData, Vector(1, 0))
    assert(res == 0.0)
  }
}
