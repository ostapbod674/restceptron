import com.perceptron.neural_network.activation_functions.{ActivationFunction, Relu, Sigmoid}
import com.perceptron.neural_network.constants.ActivationFunctionEnum
import org.scalatest.flatspec.AnyFlatSpec

class ActivationFunctionsTest extends AnyFlatSpec {
  val testRange: Vector[Double] = Vector.range(-10, 10, 2).map(_ / 10.0)

  "Sigmoid" should "must be calculated successfully" in {
    val expectedResults = Vector(0.2689414213699951, 0.31002551887238755, 0.35434369377420455, 0.401312339887548,
      0.45016600268752216, 0.5, 0.549833997312478, 0.598687660112452, 0.6456563062257954, 0.6899744811276125)
    assert(testRange.map(Sigmoid.apply) == expectedResults)
  }

  "Relu" should "must be calculated successfully" in {
    val expectedResults = Vector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.4, 0.6, 0.8)
    assert(testRange.map(Relu.apply) == expectedResults)
  }

  "Sigmoid.derivative" should "must be calculated successfully" in {
    val expectedResults = Vector(-2.0, -1.4400000000000002, -0.96, -0.5599999999999999, -0.24, 0.0,
      0.16000000000000003, 0.24, 0.24, 0.15999999999999998)
    assert(testRange.map(Sigmoid.derivative) == expectedResults)
  }

  "Relu.derivative" should "must be calculated successfully" in {
    val expectedResults = Vector(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0)
    assert(testRange.map(Relu.derivative) == expectedResults)
  }

  "ActivationFunction.getActivationFunction" should "return appropriate function" in {
    object baseMock extends ActivationFunction
    val enums = Vector(ActivationFunctionEnum.sigmoid, ActivationFunctionEnum.relu)
    assert(enums.map(baseMock.getActivationFunction).==(Vector(Sigmoid, Relu)))
  }

}
