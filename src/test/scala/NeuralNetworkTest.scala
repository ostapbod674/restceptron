import breeze.linalg.{DenseMatrix, DenseVector}
import com.perceptron.neural_network.constants.ActivationFunctionEnum
import com.perceptron.neural_network.NeuralNetwork
import com.perceptron.neural_network.constants.ActivationFunctionEnum.ActivationFunctionEnum
import org.scalatest.PrivateMethodTester
import org.scalatest.flatspec.AnyFlatSpec

import math.abs
import scala.reflect.ClassTag

class MockedNeuralNetwork(layers: Vector[Int], learningStep: Double = 0.01,
                          activationFunctionType: ActivationFunctionEnum)
  extends NeuralNetwork(layers, learningStep, activationFunctionType) {
  override def shuffle[T: ClassTag](X: Vector[T]): Vector[T] = X
}

class NeuralNetworkTest extends AnyFlatSpec with PrivateMethodTester {
  val Esp = 0.01
  val nn: NeuralNetwork = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)


  "NuralNetwork.predict" should "return result after over weights multiplication" in {
    val layers = Vector(3, 4, 1)
    val localNn = NeuralNetwork(Vector(3, 4, 1), 0.2,
      ActivationFunctionEnum.sigmoid, userWeights = Option(nn.randomWeightsForLayers(layers).zipWithIndex.map {
      case (layerWeight, index) => DenseMatrix.fill[Double](layerWeight.rows, layerWeight.cols)(index + 1)
    }))
    val actualResult = localNn.predict(Vector(0.5, 1, -0.8))
    val count = actualResult.zip(Vector(0.9997872855891814)).map(p => abs(p._1 - p._2)).count(_ > Esp)
    assert(count == 0)
  }

  "NuralNetwork.getBatchSize" should "calculate batch size based on provided input Vector, in range" in {
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val getBatchSize = PrivateMethod[Int](Symbol("getBatchSize"))
    assert(1 == nn.invokePrivate(getBatchSize(1, 3)))
  }

  "NuralNetwork.getBatchSize" should "calculate batch size based on provided input Vector, out range over top" in {
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val getBatchSize = PrivateMethod[Int](Symbol("getBatchSize"))
    assert(3 == nn.invokePrivate(getBatchSize(5, 3)))
  }

  "NuralNetwork.getBatchSize" should "calculate batch size based on provided input Vector, out range over bottom" in {
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val getBatchSize = PrivateMethod[Int](Symbol("getBatchSize"))
    assert(4 == nn.invokePrivate(getBatchSize(-1, 4)))
  }

  "NuralNetwork.cycle" should "cycle through sequence infinitely" in {
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val cycle = PrivateMethod[Seq[Int]](Symbol("cycle"))
    val originalData = Vector(1, 2, 3, 4, 5, 6, 7, 8, 9)
    assert(originalData ++ originalData == nn.invokePrivate(cycle(originalData)).take(originalData.length * 2))
  }


  "NuralNetwork.calculateLayersDerivatives" should "calculate derivatives for input values based on weights and target" in {
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val weights = nn.weights.zipWithIndex.map {
      case (layerWeight, index) => DenseMatrix.fill[Double](layerWeight.rows, layerWeight.cols)(index + 1)
    }
    val inputData = Vector(DenseVector(1.9, 0.8, -3.0, 1.0), DenseVector(-1.0, 2.9, 3.2, 4.1, 1.0), DenseVector(10.0))
    val calculateLayersDerivatives = PrivateMethod[Vector[DenseVector[Double]]](Symbol("calculateLayersDerivatives"))
    val actualResult = nn.invokePrivate(calculateLayersDerivatives(Vector(1.0), inputData, weights))
    val expectedResult = Vector(DenseVector(3240.0, 8926.199999999999, 11404.800000000001, 20590.199999999997, -0.0),
      DenseVector(-810.0))
    assert(actualResult == expectedResult)
  }


  "NuralNetwork.calculateOutputValuesForLayers" should "calculate output of each layer" in {
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val weights = nn.weights.zipWithIndex.map {
      case (layerWeight, index) => DenseMatrix.fill[Double](layerWeight.rows, layerWeight.cols)(index + 1)
    }
    val calculateOutputValuesForLayers = PrivateMethod[Vector[DenseVector[Double]]](
      Symbol("calculateOutputValuesForLayers")
    )
    val actualResult = nn.invokePrivate(calculateOutputValuesForLayers(Vector(1.9, 0.8, -3.0, 1.0), weights))
    val expectedResult = Vector(
      DenseVector(1.9, 0.8, -3.0, 1.0),
      DenseVector(0.6681877721681662, 0.6681877721681662, 0.6681877721681662, 0.6681877721681662, 0.6681877721681662),
      DenseVector(0.9987481469169387)
    )

    assert(actualResult == expectedResult)
  }

  "NuralNetwork.trainOneIteration" should "calculate weights for next iteration" in {
    val weights = nn.weights.zipWithIndex.map {
      case (layerWeight, index) => DenseMatrix.fill[Double](layerWeight.rows, layerWeight.cols)(index + 1)
    }
    val X = Vector(1.9, 0.8, -3.0, 1.0)
    val y = Vector(10.0)
    val trainOneIteration = PrivateMethod[Vector[DenseMatrix[Double]]](
      Symbol("trainOneIteration")
    )
    val actualResult = nn.invokePrivate(trainOneIteration(X, y, weights))
    val expectedResult = Vector(
      DenseMatrix(
        Vector(1.0018963424444145, 1.0007984599765956, 0.9970057750877666, 1.0009980749707446, 1.0018963424444145),
        Vector(1.0007984599765956, 0.9970057750877666, 1.0009980749707446, 1.0018963424444145, 1.0007984599765956),
        Vector(0.9970057750877666, 1.0009980749707446, 1.0018963424444145, 1.0007984599765956, 0.9970057750877666),
        Vector(1.0009980749707446, 1.0018963424444145, 1.0007984599765956, 0.9970057750877666, 1.0009980749707446)),
      DenseMatrix(2.00150397557267, 2.00150397557267, 2.00150397557267, 2.00150397557267, 2.00150397557267)
    )
    val r = actualResult.zip(expectedResult).map(
      p => p._1.toArray.zip(p._2.toArray).map(p => abs(p._2-p._1)).count(_ > Esp) == 0
    )
    assert(r.reduce(_ & _))
  }

  "NuralNetwork.fit" should "update weights" in {
    val layers = Vector(3, 4, 1)
    val localNn = NeuralNetwork(Vector(3, 4, 1), 0.2,
      ActivationFunctionEnum.sigmoid, userWeights = Option(nn.randomWeightsForLayers(layers).zipWithIndex.map {
        case (layerWeight, index) => DenseMatrix.fill[Double](layerWeight.rows, layerWeight.cols)(index + 1)
      }))
    val X = Vector(Vector(1.9, 0.8, -3.0), Vector(3.9, 4.0, 9.0))
    val y = Vector(Vector(10.0), Vector(7.0))

    val trainedNN = localNn.fit(X, y)
    val expectedResult = Vector(
      DenseMatrix(1.1711573985309909, 1.072066278952924, 0.7297515138502587, 1.0900828387038664,
        1.1711573985309909, 1.072066278952924, 0.7297515138502587, 1.0900828387038664,
        1.1711573985309909, 1.072066278952924, 0.7297515138502587, 1.0900828387038664,
        1.1711573985309909, 1.072066278952924, 0.7297515138502587, 1.0900828387038664,
        1.1711573985309909, 1.072066278952924, 0.7297515138502587, 1.0900828387038664),
      DenseMatrix(2.241433969011056, 2.241433969011056, 2.241433969011056, 2.241433969011056, 2.241433969011056)
    )

    val r = trainedNN.weights.zip(expectedResult).map(
      p => p._1.toArray.zip(p._2.toArray).map(p => abs(p._2-p._1)).count(_ > Esp) == 0
    )
    assert(r.reduce(_ & _))
  }

}
