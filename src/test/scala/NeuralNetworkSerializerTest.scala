import breeze.linalg.DenseMatrix
import com.perceptron.neural_network.activation_functions.Sigmoid
import com.perceptron.neural_network.constants.ActivationFunctionEnum
import com.perceptron.neural_network.{NeuralNetwork, NeuralNetworkSerializer}
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Properties

class EmptyMock

class NeuralNetworkSerializerTest extends AnyFlatSpec {
  "NuralNetworkSerializer" should "have the same dumped and loaded NN" in {
    assume( Properties.envOrElse("IDE_TESTS", "false").toBoolean)
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val emptyMock = new EmptyMock with NeuralNetworkSerializer
    val path = "src/test/test_data/test.nn"
    emptyMock.dump(path)(nn)
    val loadedNn = emptyMock.load(path)
    assert(loadedNn.equals(nn))
  }

  it should "have different dumped and loaded NN" in {
    assume( Properties.envOrElse("IDE_TESTS", "false").toBoolean)
    val nn = NeuralNetwork(Vector(3, 4, 1), 0.2, ActivationFunctionEnum.sigmoid)
    val emptyMock = new EmptyMock with NeuralNetworkSerializer
    val path = "src/test/test_data/test.nn"
    emptyMock.dump(path)(nn)
    val loadedNn = emptyMock.load(path)
    assert(!loadedNn.equals(nn.fit(Vector(Vector(1, 3, 4)), Vector(Vector(1)))))
  }
}
