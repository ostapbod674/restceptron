package com.perceptron.neural_network.constants

object VerbosityEnum extends Enumeration {
  type Verbosity = Value
  val Silence: Verbosity = Value(1, "Silence")
  val OnlyEpochs: Verbosity = Value(2, "OnlyEpochs")
  val EpochsWithProgress: Verbosity = Value(3, "EpochsWithProgress")
}
