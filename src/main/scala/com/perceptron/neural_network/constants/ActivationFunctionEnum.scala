package com.perceptron.neural_network.constants

object ActivationFunctionEnum  extends Enumeration {
  type ActivationFunctionEnum = Value
  val sigmoid: ActivationFunctionEnum = Value(1, "sigmoid")
  val relu: ActivationFunctionEnum = Value(2, "relu")
}
