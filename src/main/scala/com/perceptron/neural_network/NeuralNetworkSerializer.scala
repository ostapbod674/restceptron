package com.perceptron.neural_network

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

trait NeuralNetworkSerializer {
  def dump(path: String)(implicit nn: NeuralNetwork): Unit = {
    val oos = new ObjectOutputStream(new FileOutputStream(path))
    oos.writeObject(nn)
    oos.close()
  }

  def load(path: String): NeuralNetwork = {
    val ois = new ObjectInputStream(new FileInputStream(path))
    val nn = ois.readObject.asInstanceOf[NeuralNetwork]
    ois.close()
    nn
  }
}
