package com.perceptron.neural_network

import scala.collection.parallel.CollectionConverters._
import scala.language.implicitConversions
import math.pow

trait PerformanceMeasurer {

  def calculateLoss(X: Vector[Double], y: Vector[Double])(implicit nn: NeuralNetworkInterface): Double = {
    val predictions: Vector[Double] = nn.predict(X)
    predictions.zip(y).par.map(pair => pow(pair._1 - pair._2, 2)).sum
  }

  def loss(X: Vector[Vector[Double]], y: Vector[Vector[Double]])(implicit nn: NeuralNetworkInterface): Double = {
    val losses = X.zip(y).par.map(pair => calculateLoss(pair._1, pair._2))
    losses.sum * 0.5
  }

  def accuracy(X: Vector[Vector[Double]], y: Vector[Int])(implicit nn: NeuralNetworkInterface): Double = {
    val guessed: Vector[Int] = X.zip(y).par.map(pair => {
      val prediction = nn.predict(pair._1)
      if (prediction.indexOf(prediction.max) == pair._2) 1 else 0
    }).toVector
    guessed.sum.toDouble / guessed.length
  }

}
