package com.perceptron.neural_network

import com.perceptron.neural_network.constants.VerbosityEnum
import com.perceptron.neural_network.constants.VerbosityEnum.Verbosity

abstract class NeuralNetworkInterface {
  def fit(X: Vector[Vector[Double]], y: Vector[Vector[Double]], epochs: Int = 1000,
          batchSize: Int = 0, verbosity: Verbosity = VerbosityEnum.OnlyEpochs,
          verboseRate: Int = 0): NeuralNetworkInterface

  def predict(X: Vector[Double]): Vector[Double]
}
