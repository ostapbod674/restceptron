package com.perceptron.neural_network.activation_functions

import com.perceptron.neural_network.constants.ActivationFunctionEnum

trait ActivationFunction {
  def getActivationFunction(functionType: ActivationFunctionEnum.ActivationFunctionEnum): ActivationFunctionInterface = {
    functionType match {
      case ActivationFunctionEnum.sigmoid => Sigmoid
      case ActivationFunctionEnum.relu => Relu
    }

  }
}
