package com.perceptron.neural_network.activation_functions

import scala.math.exp

object Sigmoid extends ActivationFunctionInterface {
  override def apply(x: Double): Double = 1.0 / (1 + exp(-x))

  override def derivative(x: Double): Double = x * (1 - x)
}
