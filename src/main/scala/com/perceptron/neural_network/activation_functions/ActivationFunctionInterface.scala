package com.perceptron.neural_network.activation_functions

abstract class ActivationFunctionInterface extends Serializable {
  def apply(x: Double): Double

  def derivative(x: Double): Double
}
