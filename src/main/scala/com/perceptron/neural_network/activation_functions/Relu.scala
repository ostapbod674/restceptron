package com.perceptron.neural_network.activation_functions

import math.max

object Relu extends ActivationFunctionInterface {
  override def apply(x: Double): Double = max(0, x)

  override def derivative(x: Double): Double = if (x > 0) 1 else 0
}
