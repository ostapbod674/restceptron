package com.perceptron.neural_network

import breeze.linalg.{DenseMatrix, DenseVector}
import breeze.stats.distributions.Rand
import com.typesafe.scalalogging.LazyLogging
import com.perceptron.neural_network.activation_functions.{ActivationFunction, ActivationFunctionInterface}
import com.perceptron.neural_network.constants.ActivationFunctionEnum.ActivationFunctionEnum
import com.perceptron.neural_network.constants.VerbosityEnum
import com.perceptron.neural_network.constants.VerbosityEnum.Verbosity

import scala.math.sqrt
import scala.reflect.ClassTag
import scala.util.Random
import scala.collection.parallel.CollectionConverters._
import scala.language.implicitConversions

case class NeuralNetwork(layers: Vector[Int], learningStep: Double = 0.01,
                         activationFunctionType: ActivationFunctionEnum,
                         userWeights: Option[Vector[DenseMatrix[Double]]] = None)
  extends NeuralNetworkInterface with PerformanceMeasurer with ActivationFunction with LazyLogging {
  val weights: Vector[DenseMatrix[Double]] = userWeights.getOrElse(randomWeightsForLayers(layers))
  val activationFunction: ActivationFunctionInterface = getActivationFunction(activationFunctionType)

  def randomWeightsForLayers(localLayers: Vector[Int]): Vector[DenseMatrix[Double]] = {
    val innerMatrices = 0.until(localLayers.length - 2).par.map(
      i => DenseMatrix.rand[Double](localLayers(i) + 1, localLayers(i + 1) + 1, Rand.gaussian(0, 1)) / sqrt(localLayers(i))
    )
    val weights = innerMatrices.toVector
    val index = localLayers.length - 2
    weights :+ DenseMatrix.rand[Double](localLayers(index) + 1, localLayers.last) / sqrt(localLayers(index))
  }

  def fit(X: Vector[Vector[Double]], y: Vector[Vector[Double]], epochs: Int = 1000,
          batchSize: Int = 0, verbosity: Verbosity = VerbosityEnum.OnlyEpochs, verboseRate: Int = 0): NeuralNetwork = {
    logger.info("Fitting process started")
    val xWithBias: Vector[Vector[Double]] = X.map(_ :+ 1.0)
    val localBatchSize = getBatchSize(batchSize, X.length)
    val batches = cycle(xWithBias.zip(y).grouped(localBatchSize).toSeq).take(epochs)
    val newWeights = batches.zipWithIndex.foldLeft(weights) {
      case (generalWeights, (batch, epoch)) =>
        val updatedWeightsWithBatches = shuffle(batch.zipWithIndex).foldLeft(generalWeights) {
          case (iterWeights, ((x_train, y_train), batchIndex)) =>
            val updatedWeights = trainOneIteration(x_train, y_train, iterWeights)
            val progress = batchIndex.toDouble / batch.length * 100
            if (verbosity >= VerbosityEnum.EpochsWithProgress && progress % 10 == 0) {
              logger.info(s"Progress: $progress%")
            }
            updatedWeights
        }
        if (verbosity >= VerbosityEnum.OnlyEpochs && verboseRate != 0
          && ((epoch + 1) % verboseRate == 0.0 || epoch == 0)) {
          logger.info(s"Epoch: ${epoch + 1}, loss: ${loss(X, y)(this)}")
        }
        updatedWeightsWithBatches
    }
    NeuralNetwork(layers,learningStep, activationFunctionType, Option(newWeights))
  }

  private def cycle[T](seq: Seq[T]): LazyList[T] = LazyList.from(0).flatten(_ => seq.to(LazyList))

  private def getBatchSize(batch: Int, maxSize: Int): Int = {
    if (batch > 0 && batch <= maxSize) {
      batch
    } else maxSize
  }

  def shuffle[T: ClassTag](X: Vector[T]): Vector[T] = {
    val shuffled_indexes = Random.shuffle(X.indices.toList)
    shuffled_indexes.map(X(_)).toVector
  }

  implicit def VectorToDenseVector[T: ClassTag](Vector: Vector[T]): DenseVector[T] = DenseVector[T](Vector: _*)

  private def trainOneIteration(X: Vector[Double], y: Vector[Double],
                                currentWeights: Vector[DenseMatrix[Double]]): Vector[DenseMatrix[Double]] = {
    val outputValues: Vector[DenseVector[Double]] = calculateOutputValuesForLayers(X, currentWeights)
    val layerDerivatives = calculateLayersDerivatives(y, outputValues, currentWeights)

    currentWeights.indices.par.map(layerIndex => {
      val weightImprovementDelta = outputValues(layerIndex) * layerDerivatives(layerIndex).t
      val res = currentWeights(layerIndex) - weightImprovementDelta.map(_ * learningStep)
      res
    }).toVector
  }


  private def calculateOutputValuesForLayers(X: Vector[Double],
                                             localWeights: Vector[DenseMatrix[Double]]): Vector[DenseVector[Double]] = {
    localWeights.scanLeft(X.toDenseVector)((inputValues, matrix) => {
      (inputValues.toDenseMatrix * matrix).map(activationFunction.apply).toDenseVector
    })
  }

  private def calculateLayersDerivatives(y: Vector[Double],
                                         outputValues: Vector[DenseVector[Double]],
                                         localWeights: Vector[DenseMatrix[Double]]): Vector[DenseVector[Double]] = {
    val range = (outputValues.length - 2).to(1, -1)
    Vector.from(range.map(outputValues.apply).zip(range.map(localWeights.apply)).scanLeft(
      ((outputValues.last - y.toDenseVector) * outputValues.last.map(activationFunction.derivative).toDenseVector).toDenseVector
    ) {
      case (prev, (outputValue, localWeight)) =>
        val calculatedActivationFuncDerivative = outputValue.map(activationFunction.derivative).toDenseVector
        val layerDerivative: DenseVector[Double] = (
          prev.toDenseMatrix * localWeight.t
          ).toDenseVector
        (layerDerivative * calculatedActivationFuncDerivative).toDenseVector
    }).reverse
  }


  def predict(X: Vector[Double]): Vector[Double] = {
    Vector.from[Double](weights.foldLeft((X :+ 1.0).toDenseVector)((currentResult, layerWeightsMatrix) => {
      (currentResult.t * layerWeightsMatrix).t.map(activationFunction.apply).toDenseVector
    }).toArray)
  }

  def equals(obj: NeuralNetwork): Boolean = {
    this.layers == obj.layers && this.learningStep == obj.learningStep && this.weights == obj.weights
  }

}
