package com.perceptron.neural_network.nn_training.mnist

import java.awt.Image
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import scala.language.implicitConversions
import scala.collection.parallel.CollectionConverters._

trait mnistDatasetLoader {


  def loadDataset(path: String): Option[Vector[ImageVectorAndTarget]] = {
    val labeledFolders = getMnistLabeledDirectories(new File(path)).sortBy(_.getName.toInt)
    if (labeledFolders.map(_.getName.toInt).sorted == 0.to(9)) {
      val dataset = labeledFolders.par.flatMap(imageFolder => {
        imageFolder.listFiles().par.filter(f => f.getName.endsWith(".jpg")).map(imageFile => {
          val targetNumber = imageFolder.getName.toInt
          val target = createTargetVector(targetNumber)
          ImageVectorAndTarget(imageFile, target, imageFile.getName, targetNumber)
        })
      })
      Option(dataset.toVector)
    }
    else None
  }

  def createTargetVector(targetPosition: Int, size: Int = 10, defaultValue: Double = 0.0): Vector[Double] = {
    val targetLayer = Array.fill[Double](size)(defaultValue)
    targetLayer(targetPosition) = 1.0
    Vector.from(targetLayer)
  }

  def getMnistLabeledDirectories(folder: File): Vector[File] = {
    Vector.from(folder.listFiles().filter(_.isDirectory).filter(f => "[0-9]".r.findFirstIn(f.getName).isDefined))
  }

  implicit def convertImageFileToVector(imageFile: File): Vector[Double] = {
    val loadedImage: Image = ImageIO.read(imageFile).getScaledInstance(8, 8, Image.SCALE_DEFAULT)
    val buffered = new BufferedImage(8, 8, BufferedImage.TYPE_USHORT_GRAY)
    buffered.getGraphics.drawImage(loadedImage, 0, 0, null)
    val dataDuffer = buffered.getData.getDataBuffer
    val res = (for (i <- 0.until(dataDuffer.getSize)) yield dataDuffer.getElem(i).toDouble).toVector
    normalizeVector(res)
  }

  def normalizeVector(Vector: Vector[Double]): Vector[Double] = {
    val (min, max) = Vector.map(x => (x, x)).reduceLeft((x, y) => (x._1 min y._1, x._2 max y._2))
    Vector.map(_ - min).map(_ / (max - min))
  }
}

