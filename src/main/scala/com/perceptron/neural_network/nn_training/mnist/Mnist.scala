package com.perceptron.neural_network.nn_training.mnist

import com.perceptron.neural_network.constants.{ActivationFunctionEnum, VerbosityEnum}
import com.perceptron.neural_network.{NeuralNetwork, NeuralNetworkSerializer, PerformanceMeasurer}
import com.typesafe.scalalogging.LazyLogging

import java.io._
import scala.language.implicitConversions
import scala.util.{Properties, Random}

case class ImageVectorAndTarget(image: Vector[Double], target: Vector[Double], imageName: String, targetInt: Int)


object Mnist extends App with mnistDatasetLoader with PerformanceMeasurer
  with NeuralNetworkSerializer with LazyLogging {
  val nn: NeuralNetwork = NeuralNetwork(Vector(64, 32, 16, 10),
    activationFunctionType = ActivationFunctionEnum.sigmoid)
  logger.info("Loading datasets...")
  val mnistFolderPath = Properties.envOrElse("MNIST_ABS_PATH", "./mnist")
  val test: Option[Vector[ImageVectorAndTarget]] = loadDataset(mnistFolderPath + "/test")
  logger.info("Test dataset loaded")
  val train: Option[Vector[ImageVectorAndTarget]] = loadDataset(mnistFolderPath + "/train")
  logger.info("Train dataset loaded")

  if (train.isDefined && test.isDefined) {
    logger.info("Datasets loaded. Starting to train network...")
    val size = train.get.length
    val shuffledIndexes = Random.shuffle(0.until(size).toList)
    val trainX = train.get.map(_.image)
    val trainTarget = train.get.map(_.target)

    val shuffledX = shuffledIndexes.map(trainX(_)).toVector
    val shuffledY = shuffledIndexes.map(trainTarget(_)).toVector
    shuffledX.zip(shuffledY).grouped(shuffledX.length).zipWithIndex.foreach {
      case (pair, index) =>
        logger.info(s"Era $index training")
        val (x, y) = pair.unzip
        implicit val trained_nn: NeuralNetwork = nn.fit(x, y, 1000,
          batchSize = x.length, VerbosityEnum.OnlyEpochs,
          verboseRate = 100)
        val rawTestData = test.get.map(_.image)
        logger.info(s"Test accuracy: ${accuracy(rawTestData, test.get.map(_.targetInt))}, " +
          s"loss: ${loss(rawTestData, test.get.map(_.target))}")
        dump("/Users/ostapbodnar/Desktop/scala_resceptron/nn_mnist_14_04_2022")
    }
  }
  else {
    logger.info("No train or test datasets provided")
  }
}


object MnistLoadNN extends App with mnistDatasetLoader with PerformanceMeasurer
  with NeuralNetworkSerializer with LazyLogging {
  implicit val nn: NeuralNetwork = load("/Users/ostapbodnar/Desktop/scala_resceptron/nn_mnist_14_04_2022")
  val Vector = convertImageFileToVector(new File("/Users/ostapbodnar/Desktop/scala_resceptron/MNIST/train/9/162.jpg"))
  logger.info(nn.predict(Vector).mkString("Vector(", ", ", ")"))
  val test: Option[Vector[ImageVectorAndTarget]] = loadDataset("/Users/ostapbodnar/Desktop/scala_resceptron/MNIST/test")
  val rawTestData = test.get.map(_.image)
  logger.info(s"Test accuracy: ${accuracy(rawTestData, test.get.map(_.targetInt))}, " +
    s"loss: ${loss(rawTestData, test.get.map(_.target))}")

}
