package com.perceptron.neural_network.nn_training

import com.perceptron.neural_network.constants.ActivationFunctionEnum
import com.perceptron.neural_network.{NeuralNetwork, NeuralNetworkSerializer}
import com.typesafe.scalalogging.LazyLogging

object XorData {
  val X: Vector[Vector[Double]] = Vector(Vector(0.0, 0.0), Vector(0.0, 1), Vector(1.0, 0.0), Vector(1.0, 1.0))
  val y: Vector[Vector[Double]] = Vector(Vector(0.0), Vector(1.0), Vector(1.0), Vector(0.0))
}

object XorNN extends App with NeuralNetworkSerializer with LazyLogging {
  val nn = NeuralNetwork(Vector(2, 4, 2, 1), 0.5, ActivationFunctionEnum.sigmoid)

  val trainedNN = nn.fit(XorData.X, XorData.y, 10000, verboseRate = 1000)
  for ((x, target) <- XorData.X.zip(XorData.y)) {
    val pred = trainedNN.predict(x)
    logger.info(s"Input: ${x.mkString("Vector(", ", ", ")")}, actual y: ${target(0)}, pred: ${pred(0)}")
  }

  dump("/Users/ostapbodnar/Desktop/xor")(trainedNN)
}


object XorNNLoaded extends App with NeuralNetworkSerializer with LazyLogging {
  val nn = load("/Users/ostapbodnar/Desktop/xor")
  XorData.X.zip(XorData.y).foreach { case (x, target) =>
    val predicted = nn.predict(x)
    logger.info(s"Input: ${x.mkString("Vector(", ", ", ")")}, actual y: ${target(0)}, pred: ${predicted(0)}")
  }

}