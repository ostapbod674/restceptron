ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.0"
val http4sVersion = "0.23.11"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.11" % Test,
  "org.scalatestplus" %% "mockito-3-4" % "3.2.10.0" % Test,
  "org.scalanlp" %% "breeze" % "2.0",
  "org.scalanlp" %% "breeze-natives" % "2.0",
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
  "ch.qos.logback" % "logback-classic" % "1.2.11",
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-ember-server" % http4sVersion,
  "org.http4s" %% "http4s-ember-client" % http4sVersion,
  "commons-io" % "commons-io" % "20030203.000550",
  "org.powermock" % "powermock-api-mockito2" % "2.0.9" % Test,
  "org.powermock" % "powermock-module-javaagent" % "2.0.9" % Test,
  "org.powermock" % "powermock-module-junit4-rule-agent" % "2.0.9" % Test,
)

lazy val root = (project in file("."))
  .settings(
    name := "Restceptron"
  )
